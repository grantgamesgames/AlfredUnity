-- alfred_script("p")
on alfred_script(q)
	set cmd to ""
	set isRunning to ""
	tell application "System Events"
		tell application "Unity" to activate
		tell process "Unity"
			tell menu bar 1
				tell menu "Assets"
					click menu item "Refresh "
				end tell
				-- 
				-- Play
				-- 
				if q is "play" or q is "p" then
					tell menu "Edit"
						click menu item "Play "
						set cmd to "Play"
					end tell
					-- 
					-- Build & Run
					-- 
				else if q is "run" or q is "r" or q is "build" or q is "br" or q is "b" then
					tell menu "File"
						click menu item "Build & Run"
						set cmd to "Build & Run"
					end tell
				else
					set cmd to "Command not found:" & q
				end if
			end tell
		end tell
		
		set isRunning to (count of (every process whose bundle identifier is "com.Growl.GrowlHelperApp")) > 0
	end tell
	
	if isRunning then
		tell application id "com.Growl.GrowlHelperApp"
			set the allNotificationsList to ¬
				{"Alfred Unity"}
			set the enabledNotificationsList to ¬
				{"Alfred Unity"}
			register as application ¬
				"Alfred Unity" all notifications allNotificationsList ¬
				default notifications enabledNotificationsList ¬
				icon of application "Unity"
			notify with name ¬
				"Alfred Unity" title ¬
				"Running Command:" description ¬
				cmd ¬
					application name "Alfred Unity"
		end tell
	end if
	
end alfred_script
